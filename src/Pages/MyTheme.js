import React, { Component } from 'react';
import { Container, Header, H3, Tab, Tabs, TabHeading, Icon, Text, Left, Right, List, ListItem, View, Content, Thumbnail, Button } from 'native-base';
import { Image, Alert, TouchableHighlight, Picker, StyleSheet } from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import Theme from '../../Styles/Theme';
import Ionicons from 'react-native-vector-icons/Ionicons';

export default class MyTheme extends Component {
    static navigationOptions = {
        title: 'ธีมของฉัน',
        headerLeft: null,
        headerRight: <Text style={{ color: 'white', paddingRight: 15 }}>ร้าน</Text>,
    }
    render() {
        return (
            <Container>
                <ListItem style={{flexDirection:'row'}} onPress={() => {this.props.navigation.navigate('ThemeDetail')}}>
                    <View>
                        <Thumbnail square style={styles.icon} source={require('../Resources/Image/theme1-1.png')} />
                    </View>
                    <View style={{ paddingLeft: 20 }}>
                        <Text style={{fontWeight:'bold'}}>ดั้งเดิม</Text>
                        <Text note style={{ fontSize: 12, alignSelf: 'flex-start',marginTop:5 }}>ไม่มีวันหมดอายุการใช้งาน</Text>
                        <Button disabled style={[{ marginTop:10, width: 75 ,height:30},Theme.backgroundGrey]}>
                            <Entypo name='check' style={{ paddingHorizontal: 12, fontSize: 12 }}>ใช้อยู่</Entypo>
                        </Button>
                    </View>
                    <Right style={{flex:1}}>
                        <Ionicons style={{textAlign:'right'}} name='ios-arrow-forward' iconColor='#e6efe' size={12} />
                    </Right>
                </ListItem>
                <ListItem style={{flexDirection:'row'}}>
                    <View>
                        <Thumbnail square style={styles.icon} source={require('../Resources/Image/theme2-1.png')} />
                    </View>
                    <View style={{ paddingLeft: 20 }}>
                        <Text style={{fontWeight:'bold'}}>โคนี่</Text>
                        <Text note style={{ fontSize: 12, alignSelf: 'flex-start',marginTop:5 }}>ไม่มีวันหมดอายุการใช้งาน/V1.85</Text>
                        <Button style={[{ marginTop:10, width: 95, height: 30},Theme.backgroundGreen]}>
                            <Text style={{ paddingHorizontal: 1, fontSize: 12}}>ดาวน์โหลด</Text>
                        </Button>
                    </View>
                    <Right style={{flex:1}}>
                        <Ionicons style={{textAlign:'right'}} name='ios-arrow-forward' iconColor='#e6efe' size={12} />
                    </Right>
                </ListItem>
            </Container>
        )
    }
}
const styles = StyleSheet.create({
    icon: {
        width: 60,
        height: 100,
    }
})