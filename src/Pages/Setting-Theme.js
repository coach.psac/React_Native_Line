import React, { Component } from 'react';
import { Container, Header, Content, Card, CardItem, Text, Icon, Right, Separator, List, ListItem, Thumbnail } from 'native-base';
import { Image, StyleSheet } from 'react-native';

export default class Setting extends Component {
    constructor(props){
        super(props);
    }
    static navigationOptions = {
        title: 'ธีม',
        headerLeft:null,
      }
    render() {
        return (
            <Container>
                <Content>

                    <Separator style={styles.Separator}>
                        <Text style={styles.textSeparator}>ธีม</Text>
                    </Separator>
                    <ListItem onPress={() => {this.props.navigation.navigate('MyTheme')}}>
                        <Text style={styles.label}>ธีมของฉัน</Text>
                    </ListItem>
                    <ListItem>
                        <Text style={styles.label}>ประวัติการซื้อ</Text>
                    </ListItem>
                    <ListItem>
                        <Text style={styles.label}>กล่องของขวัญ</Text>
                    </ListItem>

                </Content>
            </Container>
        )
    }
}



const styles = StyleSheet.create({
    icon: {
        width: 25,
        height: 25
    },
    label: {
    },
    Separator: {
        backgroundColor: 'transparent',
        marginTop: 20
    },
    textSeparator:{
        color: 'darkblue', 
        fontSize: 12 
    },
    header:{
        backgroundColor:'#00264d',
    }

})