import React, { Component } from 'react';
import { Container, Header, H3, Tab, Tabs, TabHeading, Icon, Text, Left, Right, List, ListItem, View, Content, Thumbnail } from 'native-base';
import { Image, Alert, TouchableHighlight, Picker } from 'react-native';
import Theme from '../../Styles/Theme';
import MCI from 'react-native-vector-icons/MaterialCommunityIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import Awesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
export default class Keep extends Component {
    static navigationOptions = {
        header: null,
    }
    render() {
        return (
            <Container>
                <Header hasTabs style={Theme.backgroundPrimary}>
                    <Left>
                        <Text style={Theme.mainTextHeader}>Keep</Text>
                    </Left>
                    <Right>
                        <Feather name='search' color='whitesmoke' size={25} style={{ marginRight: 40 }} />
                        <MCI name='plus' color='whitesmoke' size={26} style={{ marginRight: 40 }} />
                        <MaterialIcons name='more-vert' color='whitesmoke' size={25} style={{ marginRight: 10 }} />
                    </Right>
                </Header>
                <Tabs tabBarUnderlineStyle={{ backgroundColor: 'white' }}>
                    <Tab heading={<TabHeading style={Theme.backgroundPrimary}><Text>ทั้งหมด</Text></TabHeading>}>
                        <KeepAll />
                    </Tab>
                    <Tab heading={<TabHeading style={Theme.backgroundPrimary}><Text>รูป</Text></TabHeading>}>
                        <KeepPhoto />

                    </Tab>
                    <Tab heading={<TabHeading style={Theme.backgroundPrimary}><Text>ข้อความ</Text></TabHeading>}>
                        <KeepText />
                    </Tab>
                    <Tab heading={<TabHeading style={Theme.backgroundPrimary}><Text>ไฟล์</Text></TabHeading>}>
                        <KeepFile />
                    </Tab>
                </Tabs>
            </Container>
        )
    }
}

class KeepPhoto extends Component {
    render() {
        return (
            <View style={{ flexDirection: 'row' }}>
                <View style={[{ flex: 1 }]}>
                    <Image style={{ width: 150, height: 150 }} source={require('../Resources/Image/car-smash.jpg')} />
                </View>
                <View style={[{ flex: 1 }]}>
                    <Image style={{ width: 150, height: 150 }} source={require('../Resources/Image/tlek.jpg')} />
                </View>
                <View style={[{ flex: 1 }]}>
                    <Image style={{ width: 150, height: 150 }} source={require('../Resources/Image/samuch.jpg')} />
                </View>
            </View>
        )
    }
}
class KeepText extends Component {
    render() {
        return (
            <View>
                <ListItem>
                    <Text note style={{ fontSize: 12 }}>2018. 03</Text>
                </ListItem>
                <ListItem style={{ flexDirection: 'column' }}>
                    <Text style={{ color: 'darkblue', alignSelf: 'flex-start' }}>https://news.kapook.com/topics/ป้าทุบรถ</Text>
                    <Text note style={{ fontSize: 12, alignSelf: 'flex-start' }}>21 มี.ค. 2018</Text>
                </ListItem>
            </View>
        )
    }
}
class KeepFile extends Component {
    render() {
        return (
            <View>
                <ListItem>
                    <Text note style={{ fontSize: 12 }}>2018. 03</Text>
                </ListItem>
                <ListItem style={{}}>
                    <Thumbnail style={{ width: 40, height: 40 }} source={require('../Resources/Image/word-doc.png')} />
                    <View style={{ paddingLeft: 20 }}>
                        <Text>Angry-Aunt.docx</Text>
                        <Text note style={{ fontSize: 12, alignSelf: 'flex-start' }}>20 มี.ค. 2018</Text>
                    </View>
                </ListItem>
            </View>
        )
    }
}
class KeepAll extends Component {
    render() {
        return (
            <View>
                <ListItem>
                    <Text note style={{ fontSize: 12 }}>2018. 03</Text>
                </ListItem>
                <ListItem style={{}}>
                    <Image style={{ width: 50, height: 50 }} source={require('../Resources/Image/car-smash.jpg')} />
                    <View style={{ paddingLeft: 10 }}>
                        <Text>car-smash.jpg</Text>
                        <Text note style={{ fontSize: 12, alignSelf: 'flex-start' }}>22 มี.ค. 2018</Text>
                    </View>
                </ListItem>
                <ListItem style={{}}>
                    <Image style={{ width: 50, height: 50 }} source={require('../Resources/Image/tlek.jpg')} />
                    <View style={{ paddingLeft: 10 }}>
                        <Text>tlek.jpg</Text>
                        <Text note style={{ fontSize: 12, alignSelf: 'flex-start' }}>22 มี.ค. 2018</Text>
                    </View>
                </ListItem>
                <ListItem style={{}}>
                    <Image style={{ width: 50, height: 50 }} source={require('../Resources/Image/samuch.jpg')} />
                    <View style={{ paddingLeft: 10 }}>
                        <Text>samuch.jpg</Text>
                        <Text note style={{ fontSize: 12, alignSelf: 'flex-start' }}>22 มี.ค. 2018</Text>
                    </View>
                </ListItem>
                <ListItem style={{ flexDirection: 'column' }}>
                    <Text style={{ color: 'darkblue', alignSelf: 'flex-start' }}>https://news.kapook.com/topics/ป้าทุบรถ</Text>
                    <Text note style={{ fontSize: 12, alignSelf: 'flex-start' }}>21 มี.ค. 2018</Text>
                </ListItem>
                <ListItem style={{}}>
                    <Thumbnail style={{ width: 40, height: 40 }} source={require('../Resources/Image/word-doc.png')} />
                    <View style={{ paddingLeft: 20 }}>
                        <Text>Angry-Aunt.docx</Text>
                        <Text note style={{ fontSize: 12, alignSelf: 'flex-start' }}>20 มี.ค. 2018</Text>
                    </View>
                </ListItem>
            </View>
        )
    }
}