import React, { Component } from 'react';
import { Content, Body, Container, Header, Tab, Tabs, TabHeading, Icon, Text, Left, Right, Thumbnail, List, ListItem, View, sch } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
// import MoreItems from './More/MoreItems';
import { Image, TouchableHighlight } from 'react-native';
import Theme from '../../Styles/Theme'
export default class More extends Component {
    render() {
        let { navigation } = this.props;
        return (
            <Content>
                <MoreItems item_1={require('./../Resources/Image/user.jpg')}
                    item_2={require('./../Resources/Icon/more_point.png')}
                    item_3={require('./../Resources/Icon/more_qr.png')}
                    navigation={this.props.navigation} 
                    header />
                <View style={{ flex: 1, borderBottomWidth: 1, borderColor: '#F0F0F0' }} />
                <MoreItems item_1={require('./../Resources/Icon/more_add_friends.png')}
                    item_2={require('./../Resources/Icon/more_sticker_shop.png')}
                    item_3={require('./../Resources/Icon/more_theme_shop.png')}
                    text_1='Add friends'
                    text_2='Sticker Shop'
                    text_3='Theme Shop' />
                <MoreItems item_1={require('./../Resources/Icon/more_official_accounts.png')}
                    item_2={require('./../Resources/Icon/more_pay.png')}
                    item_3={require('./../Resources/Icon/more_line_tv.png')}
                    text_1='Official Accounts'
                    text_2='Rabbit LINE Pay'
                    text_3='LINE TV' />
                <MoreItems item_1={require('./../Resources/Icon/more_line_today.png')}
                    item_2={require('./../Resources/Icon/more_line_man.png')}
                    item_3={require('./../Resources/Icon/more_giftshop.png')}
                    text_1='LINE TODAY'
                    text_2='LINE MAN'
                    text_3='GIFTSHOP' />
                <MoreItems item_1={require('./../Resources/Icon/more_line_mobile.png')}
                    item_2={require('./../Resources/Icon/more_keep.png')}
                    item_3={require('./../Resources/Icon/more_nearby.png')}
                    text_1='LINE MOBILE'
                    text_2='Keep'
                    text_3='Nearby'
                    navigation={this.props.navigation} />
                <View style={{ flex: 1, marginTop: 20, borderBottomWidth: 1, borderColor: '#F0F0F0' }} />
            </Content>
        );
    }
}

class MoreItems extends Component {
    render() {
        let { header, item_1, item_2, item_3, text_1, text_2, text_3, navigation } = this.props;
        let color = '#FFFFFF';
        let size = 80;
        if (header) {
            color = '#F7F7F7';
            size = 70;
        }
        return (
            <View style={{ height: size, backgroundColor: color }}>
                <View style={{ flexDirection: 'row', marginTop: 15 }}>
                    <TouchableHighlight style={{ flex: 1 }} underlayColor='transparent' onPress={() => {
                        if (item_1 == require('./../Resources/Image/user.jpg')) {
                            navigation.navigate('Profile')
                        }
                    }}>
                        <View style={[{ flex: 1 }, Theme.center]}>
                            {header ? <Thumbnail small source={item_1} /> : <Image style={{ width: 35, height: 35 }} resizeMode='contain' source={item_1} />}
                            <Text style={Theme.moreText}>{text_1}</Text>
                        </View>
                    </TouchableHighlight>

                    <TouchableHighlight style={{ flex: 1 }} underlayColor='transparent' onPress={() => {
                        if (text_2 == 'Keep') {
                            navigation.navigate('Keep')
                        }
                    }}>
                        <View style={[Theme.center]} >
                            <Image style={{ width: 35, height: 35 }} resizeMode='contain' source={item_2} />
                            <Text style={Theme.moreText}>{text_2}</Text>
                        </View>
                    </TouchableHighlight>
                    <View style={[{ flex: 1 }, Theme.center]}>
                        <Image style={{ width: 35, height: 35 }} resizeMode='contain' source={item_3} />
                        <Text style={Theme.moreText}>{text_3}</Text>
                    </View>
                </View>
            </View>
        );
    }
}