import React, { Component } from 'react';
import { Badge,Content,Container, Header, Tab, Tabs, TabHeading,Text, Icon, Left, Right, List, ListItem,View, Thumbnail, Body } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
export default class Chats extends Component {
    state = {  }
    render() {
        return (
            <Container>
                <List>
                  <ChatItems source={require('./../Resources/Image/fuse.jpg')} name='Fuse Rhazic' message='ทำเสร็จยังแวร้ ?' time='4:22 PM'/>
                  <ChatItems source={require('./../Resources/Image/tlek.jpg')} name='Tlek Metta' message='โจ้ว ๆๆ' no='2' time='2.10 PM'/>
                  <ChatItems source={require('./../Resources/Image/tide.jpg')} name='Keatiyos Yothinraungrong' message='อยากไปขอลายเซ็น Lakelz' no='4' time='1.00 PM'/>
                  <ChatItems source={require('./../Resources/Image/samuch.jpg')} name='สะสะสะสาา มารถ'/>
                </List>
            </Container>
        );
    }
}

class ChatItems extends Component {
    render() {
        let {source,name,message,no,time} = this.props
        return (
            <ListItem avatar>
                <Left>
                    <Thumbnail source={source}/>
                </Left>
                <Body>
                    <View style={{flexDirection:'row'}}>
                        <View style={{justifyContent:'center',marginTop:5,marginBottom:5}}>
                            <Text>{name}</Text>
                            <Text note>{message}</Text>
                        </View>
                        <View style={{flex:1,paddingRight:10}}>
                            <Text style={{textAlign:'right'}} note>{time}</Text>
                            {no?
                                <Badge style={{backgroundColor:'#53B535',marginTop:5,alignSelf:'flex-end'}}>
                                    <Text>{no}</Text>
                                </Badge>
                            :null}
                        </View>
                    </View>
                </Body>
            </ListItem>
        );
    }
}