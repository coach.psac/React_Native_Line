import React, { Component } from 'react';
import {DeckSwiper, Container,Content, Header, Tab, Tabs, TabHeading, Icon, Text ,Left,Right,ScrollableTab,View} from 'native-base';
import { Image } from 'react-native';
export default class LineToday extends Component {
  render() {
    return (
      <Content>
        <Tabs tabBarUnderlineStyle={{backgroundColor:'black'}} renderTabBar={()=> <ScrollableTab />}>
          <Tab heading="Top" activeTabStyle={{paddingLeft:0,paddingRight:0,backgroundColor:'white'}} textStyle={{color:'grey'}} tabStyle={{paddingLeft:0,paddingRight:0,backgroundColor:'white'}} activeTextStyle={{color:'black'}} >
            <LineTodayItem/>
          </Tab>
          <Tab heading="ทั่วไป" activeTabStyle={{paddingLeft:0,paddingRight:0,backgroundColor:'white'}} textStyle={{color:'grey'}} tabStyle={{paddingLeft:0,paddingRight:0,backgroundColor:'white'}} activeTextStyle={{color:'black'}} >
            <LineTodayItem/>
          </Tab>
          <Tab heading="บันเทิง" activeTabStyle={{paddingLeft:0,paddingRight:0,backgroundColor:'white'}} textStyle={{color:'grey'}} tabStyle={{paddingLeft:0,paddingRight:0,backgroundColor:'white'}} activeTextStyle={{color:'black'}} >
            <LineTodayItem/>
          </Tab>
          <Tab heading="ทีวี" activeTabStyle={{paddingLeft:0,paddingRight:0,backgroundColor:'white'}} textStyle={{color:'grey'}} tabStyle={{paddingLeft:0,paddingRight:0,backgroundColor:'white'}} activeTextStyle={{color:'black'}} >
            <LineTodayItem/>
          </Tab>
          <Tab heading="ดูดวง" activeTabStyle={{paddingLeft:0,paddingRight:0,backgroundColor:'white'}} textStyle={{color:'grey'}} tabStyle={{paddingLeft:0,paddingRight:0,backgroundColor:'white'}} activeTextStyle={{color:'black'}} >
            <LineTodayItem/>
          </Tab>
          <Tab heading="วิดีโอ" activeTabStyle={{paddingLeft:0,paddingRight:0,backgroundColor:'white'}} textStyle={{color:'grey'}} tabStyle={{paddingLeft:0,paddingRight:0,backgroundColor:'white'}} activeTextStyle={{color:'black'}} >
            <LineTodayItem/>
          </Tab>
          <Tab heading="กีฬา" activeTabStyle={{paddingLeft:0,paddingRight:0,backgroundColor:'white'}} textStyle={{color:'grey'}} tabStyle={{paddingLeft:0,paddingRight:0,backgroundColor:'white'}} activeTextStyle={{color:'black'}} >
            <LineTodayItem/>
          </Tab>
          <Tab heading="ไลฟ์สไตล์" activeTabStyle={{paddingLeft:0,paddingRight:0,backgroundColor:'white'}} textStyle={{color:'grey'}} tabStyle={{paddingLeft:0,paddingRight:0,backgroundColor:'white'}} activeTextStyle={{color:'black'}} >
            <LineTodayItem/>
          </Tab>
        </Tabs>
      </Content>
    );
  }
}

class LineTodayItem extends Component {
  render() {
    return (
      <View>
        <Image source={require('./../Resources/Image/linetoday_highlight.png')} resizeMode='stretch' style={{width:null,height:250}}/>
        <View style={{flexDirection:'row',padding:15}}>
          <Image source={require('./../Resources/Image/linetoday_1.png')} resizeMode='stretch' style={{flex:1,width:null,height:80}}/>
          <View style={{flex:2,marginLeft:15}}>
            <Text>ส่องเรตติ้งละคร รางวัลโทรทัศน์ทองคำ ปี 60</Text>
            <Text note>TV Digital Watch</Text>
          </View>
        </View>
        <View style={{flexDirection:'row',padding:15}}>
          <Image source={require('./../Resources/Image/linetoday_2.png')} resizeMode='stretch' style={{flex:1,width:null,height:80}}/>
          <View style={{flex:2,marginLeft:15}}>
            <Text>แฉรถทัวร์บริษัทดัง ที่เต็มแต่ยังรับคนเพิ่มให้นั่งพื้น คนขับบอกดื้อๆ เดี๋ยวก็มีคนลง</Text>
            <Text note>Khaosod</Text>
          </View>
        </View>
        <View style={{flexDirection:'row',padding:15}}>
          <Image source={require('./../Resources/Image/linetoday_3.png')} resizeMode='stretch' style={{flex:1,width:null,height:80}}/>
          <View style={{flex:2,marginLeft:15}}>
            <Text>"อภิสิทธิ์" ไล่สางคนหนุน "ประยุทธ์" ไปสังกัดพรรคอื่น</Text>
            <Text note>PPTV HD 36</Text>
          </View>
        </View>
      </View>
    );
  }
}